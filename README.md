THREADED
========

Abstract local multiplayer infinite runner survival game

Made at the Berlin Mini Game Jam October 2015


Instructions
------------

Avoid the obstacles of your same color.  Adjust your size to fit in the gaps.  Narrower shapes make obstacles faster.  Last surviving player wins.

Up to 4 players. Single player practice mode included.

Accepts keyboard input and/or gamepad controllers.

Settings
--------

    F1 - toggle fullscreen (borderless window mode)
    Shift+F1 - toggle fullscreen (normal fullscreen mode)
    F2 - pause/unpause music
    F3 - reset game (go to splash screen)
    F4 - toggle sound
    F5 - toggle vsync (adjust this if the game doesn't display smoothly, depends on your monitor)



Credits
-------

    Visual Design:  Denice Wagner & Tommy Friese
    Music & SFX:  Norman Ritter
    Code:  Christiaan Janssen
    
    Game engine:  [Löve2D](htt://love2d.org)
    License (Code): [GNU GPL V3.0](http://www.gnu.org/licenses/gpl-3.0.en.html)
    Music: "Master of the Universe" copyright Alpha Boy 2012 (https://alphaboymusic.bandcamp.com/album/heroes-on-tape)
    License (Other Assets):  [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
    