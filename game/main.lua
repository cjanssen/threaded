--     Threaded: local multiplayer infinite runner game
--     Copyright (C) 2015 Christiaan Janssen

--     This program is free software: you can redistribute it and/or modify
--     it under the terms of the GNU General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.

--     This program is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU General Public License for more details.

--     You should have received a copy of the GNU General Public License
--     along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'vector'

function love.load()
    love.filesystem.load("obstacles.lua")()
    love.filesystem.load("player.lua")()
    love.filesystem.load("menu.lua")()
    love.filesystem.load("sound.lua")()

    screenWidth,screenHeight = 800,1000
    -- love.window.setMode(screenWidth,screenHeight)
    love.window.setIcon(love.graphics.newImage("img/icon.png"):getData())

    initGame()

    -- sound
    initSound()
end

function initGame()

    -- title
    title_screen = love.graphics.newImage("img/THREADED_title_screen.png")

    bigFont = love.graphics.newFont("fnt/andvari.ttf",64)
    smallFont = love.graphics.newFont("fnt/andvari.ttf",20)
    initPlayers()
    game = {
        running = true,
        winner = nil,
        timer = 0,
        delay = 4,
        dualmode = false
    }
    resetGame()
    initMenu()
    resetMenu()
end

function initPlayers()
    Players = {
        initPlayer( {
            id = 1,
            x = screenWidth*0.20,
            color = {123,255,46},
            glow = "img/p1_glow.png",
            controlTxt = "[arrows] or [gamepad 3]",
            keymap = {
                left = "left",
                right = "right",
                up = "up",
                down = "down",
                joy = 3,
            }
        }),
        initPlayer({
            id = 2,
            x = screenWidth*0.40,
            color = {19,199,237},
            glow = "img/p2_glow.png",
            controlTxt = "[wasd] or [gamepad 4]",
            keymap = {
                left = "a",
                right = "d",
                up = "w",
                down = "s",
                joy = 4,
            }
        }),
        initPlayer( {
            id = 3,
            x = screenWidth*0.60,
            glow = "img/p3_glow.png",
            controlTxt = "[ijkl] or [gamepad 1]",
            color = {205,131,1},
            keymap = {
                left = "j",
                right = "l",
                up = "i",
                down = "k",
                joy = 1,
            }
        }),
        initPlayer({
            id = 4,
            x = screenWidth*0.80,
            controlTxt = "[tfgh] or [gamepad 2]",
            color = {232,42,100},
            glow = "img/p4_glow.png",
            keymap = {
                left = "f",
                right = "h",
                up = "t",
                down = "g",
                joy = 2,
            }
        }),
    }
end

function resetGame()
    for _,player in ipairs(Players) do
        player:restart()
    end
    game.running = true
    game.winner = nil
end

function checkWinner()
    local livecount = 0
    for _,player in ipairs(Players) do
        if player.active then
            livecount = livecount + 1
        end
    end

    if livecount <= 1 then
        startWinningAnim()
    end
end

function notifyDied(victim)
    if game.dualmode then
        for _,player in ipairs(Players) do
            if player ~= victim and player.lifes > 0 then
                player.lifes = player.lifes + 1
                if player.lifes > player.maxlifes then
                    player.lifes = player.maxlifes
                end
            end
        end
    end

    local player = victim
    player.lifes = player.lifes - 1
end

function startWinningAnim()
    game.running = false
    game.timer = game.delay

    for _,player in ipairs(Players) do
        if player.active then
            game.winner = player
        end
    end

    -- 1-player situation
    if not game.winner then
        local pc = 0
        for _,player in ipairs(Players) do
            if player.enabled then
                pc = pc + 1
                game.winner = player
            end
        end
        if pc ~= 1 then game.winner = null end
    end

    -- sound
    if game.winner then
        if game.winner.active then
            playSound(Sound.sfx.win)
        else
            playSound(Sound.sfx.lose)
        end
    end
end

function love.update(dt)

    if Menu.active then
        updateMenu(dt)
        return
    end

    if game.running then
        for _,player in ipairs(Players) do
            if player.active then
               player:update(dt)
            end
        end
    else
        if game.timer > 0 then
            game.timer = game.timer - dt
            if game.timer <= 0 then
                resetGame()
            end
        end
    end
end

function drawIter()
    love.graphics.setColor(16,16,16)
    love.graphics.rectangle("fill",0,0,screenWidth, screenHeight)

    if Menu.active then
        drawMenu()
        return
    end

    for _,player in ipairs(Players) do
        if player.active then
            player:draw()
        end
    end

    if game.winner then
        love.graphics.setFont(bigFont)
        love.graphics.setColor(game.winner.color[1], game.winner.color[2], game.winner.color[3])
        local txt
        if game.winner.active then
            txt = "WINNER"
        else
            txt = "GAME OVER"
        end
        local w = bigFont:getWidth(txt)
        love.graphics.print(txt, (screenWidth-w) * 0.5, screenHeight*0.3)
    end
end

function love.draw()
    love.graphics.setColor(8,8,8)
    love.graphics.setScissor()
    love.graphics.rectangle("fill",0,0,love.graphics.getWidth(), love.graphics.getHeight())

    love.graphics.push()

    local ssw,ssh = love.graphics.getWidth(), love.graphics.getHeight()
    local sc = math.min(ssw/screenWidth, ssh/screenHeight)
    local sw,sh = screenWidth*sc, screenHeight*sc
    local ox = ssw>sw and (ssw-sw)*0.5 or 0
    local oy = ssh>sh and (ssh-sh)*0.5 or 0

    love.graphics.setScissor(ox,oy,sw,sh)

    love.graphics.translate(ox,oy)
    love.graphics.scale(sc,sc)

    drawIter()
    love.graphics.pop()
end

function toggleFullscreen()
    local w, h, f = love.window.getMode()
    f.fullscreen = not f.fullscreen
    if f.fullscreen then
        if love.keyboard.isDown("lshift") or love.keyboard.isDown("rshift") then
            f.fullscreentype = "normal"
        else
            f.fullscreentype = "desktop"
        end
    end
    love.window.setMode(w,h,f)
end

function toggleVsync()
    local w, h, f = love.window.getMode()
    f.vsync = not f.vsync
    love.window.setMode(w,h,f)
end

function love.keypressed(key)
    if key == "escape" then
        if not Menu.active then
            resetMenu()
        else
            love.event.push("quit")
        end
        return
    end

    if key == "f1" then
        toggleFullscreen()
        return
    end

    if key == "f2" then
        toggleMusic()
        return
    end

    if key == "f3" then
        resetMenu()
        return
    end

    if key == "f4" then
        toggleSound()
        return
    end

    if key == "f5" then
        toggleVsync()
        return
    end

    if Menu.active then
        keypressedMenu(key)
        return
    end

    for _,player in ipairs(Players) do
        if player.active then
            player:keypressed(key)
        end
    end
end

function love.keyreleased(key)
    for _,player in ipairs(Players) do
        if player.active then
            player:keyreleased(key)
        end
    end
end

function getJoystickNumber(joystick)
    local joysticks = love.joystick.getJoysticks()
    for i,joy in ipairs(joysticks) do
        if joy == joystick then return i end
    end
end

function applyJoystickAction(joystick, action, data)
    local joyid = getJoystickNumber(joystick)
    for _,player in ipairs(Players) do
        if player.active and player.keymap.joy == joyid then
            table.insert(data, 1, player)
            player[action](unpack(data))
        end
    end
end

function love.joystickaxis( joystick, axis, value )
    applyJoystickAction(joystick, "joyaxis", {axis, value})
end

function love.joystickhat( joystick, hat, direction)
    applyJoystickAction(joystick, "joyhat", {hat, direction})
end

function love.joystickpressed(joystick, botan)
    if Menu.active then
        joypressedMenu(getJoystickNumber(joystick))
        return
    end
    applyJoystickAction(joystick, "joypressed", {botan})
end

function love.joystickreleased(joystick, botan)
    applyJoystickAction(joystick, "joyreleased", {botan})
end
