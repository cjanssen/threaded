--     Threaded: local multiplayer infinite runner game
--     Copyright (C) 2015 Christiaan Janssen

--     This program is free software: you can redistribute it and/or modify
--     it under the terms of the GNU General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.

--     This program is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU General Public License for more details.

--     You should have received a copy of the GNU General Public License
--     along with this program.  If not, see <http://www.gnu.org/licenses/>.

function initMenu()
    Menu = {
        active = true,
        screenNdx = 1,
        initTimer = 4,
        initDelay = 5,
        autoskip = true,
    }
end

function resetMenu()
    Menu.active = true
    Menu.ready = false
    Menu.screenNdx = 1
    Menu.initTimer = Menu.initDelay

    -- reset game status
    game.dualmode = false
    for _,player in ipairs(Players) do
        player.enabled = false
    end
end

function updateMenu(dt)
    if Menu.screenNdx == 1 then
        -- nothing
        Menu.initTimer = Menu.initTimer - dt
        if Menu.initTimer <= 0  and Menu.autoskip then
            Menu.screenNdx = 2
        end
    elseif Menu.screenNdx == 2 then
        if not Menu.ready then
            local playerCount = 0
            for _,player in ipairs(Players) do
                if player.enabled then
                    playerCount = playerCount + 1
                end
            end

            if playerCount > 0 then
                -- if playerCount == 2 then game.dualmode = true end
                Menu.ready = true
            end
        end
    end
end

function printJustified(txt, minX, maxX, y, font)
    love.graphics.setFont(font)
    local words, len = {},0

    -- separate sentence into list of words with lengths
    -- special: "[" and "]" mark series that should not be split
    local word,paren = "",false
    for c in txt:gmatch(".") do
        if not paren and c == " " then
            local l = font:getWidth(word)
            len = len + l
            table.insert(words, {w = word, l = l})
            word = ""
        else
            word = word..c
            if c=="[" then
                paren = true
            elseif c=="]" then
                paren = false
            end
        end
    end
    local l = font:getWidth(word)
    len = len + l
    table.insert(words, {w = word, l = l})

    -- in case it's not a sentence or does not fit in the space, just print it centered
    local space = (maxX-minX) - len
    if space < 0 or #words < 2 then
        love.graphics.print(txt, (maxX+minX - font:getWidth(txt))*0.5, y)
        return
    end

    -- justified printing
    local singlespace = space / (#words-1)
    local x = minX
    for _,wordInfo in ipairs(words) do
        love.graphics.print(wordInfo.w, x, y)
        x = x + wordInfo.l + singlespace
    end
end

function drawMenu()
    if Menu.screenNdx == 1 then
        love.graphics.setColor(255,255,255)
        love.graphics.draw(title_screen)
    elseif Menu.screenNdx == 2 then
        for i,player in ipairs(Players) do
            local txt,w,h = "",0,0
            if player.enabled then
                love.graphics.setColor(255,255,255,64)
                love.graphics.draw(player.glow_img, 110, i*200, 0, 1, 1, player.glow_img:getWidth()*0.5, player.glow_img:getWidth()*0.5)
                love.graphics.setColor(player.color[1], player.color[2], player.color[3])
                love.graphics.setFont(bigFont)
                txt = "READY!"
                w,h = bigFont:getWidth(txt),bigFont:getHeight()
                love.graphics.print(txt, 470 - w*0.5, i*200 - h*0.5)
            else
                love.graphics.setColor(player.color[1], player.color[2], player.color[3], 64)
                love.graphics.setFont(smallFont)
                txt = string.upper(player.controlTxt .. " press to join")
                local h = smallFont:getHeight()
                printJustified(txt, 210, 740, i*200-h*0.5, smallFont)
            end
            love.graphics.rectangle("fill", 60, i*200 - 50, 100, 100)

        end

        if Menu.ready then
            love.graphics.setColor(128,128,128)
            love.graphics.setFont(smallFont)
            local txt = string.upper("Press Enter to start")
            local w,h = smallFont:getWidth(txt), smallFont:getHeight()
            love.graphics.print(txt, (screenWidth-w)*0.5, 900)
        end
    end
end

function keypressedMenu(key)
    if Menu.screenNdx == 2 then
        for _,player in ipairs(Players) do
            for _,keyCode in pairs(player.keymap) do
                if key == keyCode then
                    player.enabled = true
                    playSelect(player.id)
                end
            end
        end
    end

    if key == "return" then
        if Menu.screenNdx == 1 then
            Menu.screenNdx = 2
        elseif Menu.screenNdx == 2 and Menu.ready then
            Menu.active = false
            resetGame()
            playSound(Sound.sfx.start)
        end
    end
end

function joypressedMenu(joyid)
    if Menu.screenNdx == 2 then
        for _,player in ipairs(Players) do
            if joyid == player.keymap.joy then
                player.enabled = true
                playSelect(player.id)
            end
        end
    end
end
