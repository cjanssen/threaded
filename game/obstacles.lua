--     Threaded: local multiplayer infinite runner game
--     Copyright (C) 2015 Christiaan Janssen

--     This program is free software: you can redistribute it and/or modify
--     it under the terms of the GNU General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.

--     This program is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU General Public License for more details.

--     You should have received a copy of the GNU General Public License
--     along with this program.  If not, see <http://www.gnu.org/licenses/>.

function initObstacles(player)
    local obstacles = {}
    obstacles.list = {}
    obstacles.speed = 100
    obstacles.minSpeed = 150
    obstacles.maxSpeed = 600
    obstacles.player = player
    obstacles.density = 16
    obstacles.minDens = 15
    obstacles.maxDens = 28

    obstacles.reset = function(self)
        self.speed = self.minSpeed
        self:populate()
    end


    obstacles.populate = function(self)
        local h = screenHeight
        self.list = {}
        for i=1,self.density do
            self:spawnObstacle()
            self.list[i].pos.y = -h * i / self.density - self.list[i].size.y
        end
    end

    obstacles.spawnObstacle = function(self)
        local obstacle = {
            size = Vector(math.random(100)+25, math.random(100)+25),
            player = self.player,
            parent = self,
        }
        local sw = screenWidth - obstacle.size.x
        obstacle.pos = Vector(
            math.clamp(0, math.random(sw*1.3) - sw*0.15, sw),
            -obstacle.size.y*(1+math.random()*5))

        obstacle.update = function(self, dt)
            self.pos.y = self.pos.y + dt * self.parent.speed
            if self.pos.y > screenHeight then
                return false
            end
            return true
        end

        obstacle.draw = function(self)
            local alpha = 1
            if player.deadTimer > 0 then
                alpha = player.deadTimer / player.deadDelay
            end
            love.graphics.setColor(255,255,255, 1)
            love.graphics.draw(self.player.glow_img, self.pos.x + self.size.x*0.5, self.pos.y + self.size.y * 0.5, 0, 1, 1, self.player.glow_img:getWidth()*0.5, self.player.glow_img:getHeight()*0.5)
            love.graphics.setColor(player.color[1], player.color[2], player.color[3], 96 * alpha)
            love.graphics.rectangle("fill", math.floor(self.pos.x), math.floor(self.pos.y), math.floor(self.size.x), math.floor(self.size.y))
            -- love.graphics.line(self.pos.x, self.pos.y, self.pos.x + self.size.x, self.pos.y,
            --     self.pos.x+self.size.x, self.pos.y+self.pos.y, self.pos.x, self.pos.y+self.pos.y)
        end

        table.insert(self.list, obstacle)
    end

    obstacles.update = function(self, dt)
        if self.speed > 0 then
            for i=#self.list,1,-1 do
                if not self.list[i]:update(dt) then
                    table.remove(self.list, i)
                    if self.density > #self.list then
                        self:spawnObstacle()
                    end
                    local count = math.min(math.random(3), self.density - #self.list, 1)
                    for ii=1,count do
                        if math.random() < 0.5 then
                            self:spawnObstacle()
                        end
                    end
                end
            end
        end
    end

    obstacles.draw = function(self)
        for _,obs in ipairs(self.list) do
            obs:draw()
        end
    end

    return obstacles
end
