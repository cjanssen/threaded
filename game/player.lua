--     Threaded: local multiplayer infinite runner game
--     Copyright (C) 2015 Christiaan Janssen

--     This program is free software: you can redistribute it and/or modify
--     it under the terms of the GNU General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.

--     This program is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU General Public License for more details.

--     You should have received a copy of the GNU General Public License
--     along with this program.  If not, see <http://www.gnu.org/licenses/>.

function initPlayer(begindata)
    local player = {
        id = begindata.id,
        foot = 115,
        center = Vector(100,900),
        size = Vector(50,50),
        ratio = 0.5,
        speed = 300,
        dir = Vector(0,0),
        growth = 0,
        growSpeed = 2.5,
        deadTimer = 0,
        deadDelay = 3,
        keymap = begindata.keymap,
        color = begindata.color,
        enabled = true,
        active = true,
        maxlifes = 4,
        lifes = 4,
        fullBoxCollision = true,

        glow_img = love.graphics.newImage(begindata.glow),
        controlTxt = begindata.controlTxt,
    }
    player.obstacles = initObstacles(player)

    player.restart = function(self)
        self:newLife()
        self.lifes = self.maxlifes
        self.active = self.enabled
    end

    player.newLife = function(self)
        self.center.x = begindata.x
        self.center.y = screenHeight - self.foot
        self.ratio = 0.5
        self.deadTimer = 0
        self.obstacles:reset()
        self:recalcSize()
    end

    player.setEnabled = function(self, enabled)
        self.enabled = enabled
        if enabled then self.active = true end
    end

    player.recalcSize = function(self)
        self.size = getSizeFromRatio(self.ratio)
        -- self.obstacles.speed = self.obstacles.minSpeed + (1-self.ratio) * (self.obstacles.maxSpeed - self.obstacles.minSpeed)
        self.obstacles.speed = self.obstacles.minSpeed * math.pow(self.obstacles.maxSpeed / self.obstacles.minSpeed, (1-self.ratio))
        self.obstacles.density = self.obstacles.minDens + self.ratio * (self.obstacles.maxDens - self.obstacles.minDens)
    end

    player.update = function(self, dt)
        if self.deadTimer > 0 then
            self.deadTimer = self.deadTimer - dt
            if self.deadTimer <= 0 then
                if player.lifes <= 0 then
                    player.active = false
                    -- finally, check if someone won
                    checkWinner()
                end

                self:newLife()
            end
            return
        end

        self.obstacles:update(dt)

        -- shape
        self.ratio = self.ratio + dt * self.growSpeed * self.growth
        self.ratio = math.clamp(0, self.ratio, 1)
        if self.growth ~= 0 then
            if self.growth > 0 and self.ratio >= 1 then
                self.growth = 0
            elseif self.growth < 0 and self.ratio <= 0 then
                self.growth = 0
            end

            self:recalcSize()
        end

        -- pos
        self.center = self.center + self.dir * self.speed * dt
        self.center = self.center:clamp(Vector(self.size.x*0.5,0), Vector(screenWidth-self.size.x*0.5, screenHeight))

        -- collision
        if self:checkCollision() then
            self.deadTimer = self.deadDelay
            self.obstacles.speed = 0
            playCrash(self.id)
            notifyDied(self)
        end
    end

    player.checkCollision = function(self)
        if player.fullBoxCollision then
            local topleft = self.center - self.size * 0.5
            local bottomright = self.center + self.size * 0.5

            for _,obs in ipairs(self.obstacles.list) do
                if obs.pos.y + obs.size.y >= topleft.y and
                    obs.pos.y <= bottomright.y and
                    obs.pos.x + obs.size.x >= topleft.x and
                    obs.pos.x <= bottomright.x then
                    return true
                end
            end

            return false
        else
            local left = self.center - self.size * 0.5
            local right = left + Vector(self.size.x, 0)
            for _,obs in ipairs(self.obstacles.list) do
                if obs.pos.y + obs.size.y >= left.y and
                    obs.pos.y <= right.y and
                    obs.pos.x + obs.size.x >= left.x and
                    obs.pos.x <= right.x then
                    return true
                end
            end
            return false
        end
    end

    player.draw = function(self)
        local alpha = 1
        if self.deadTimer > 0 then
            alpha = self.deadTimer / self.deadDelay
        end

        alpha = alpha * (self.lifes / self.maxlifes)

        love.graphics.setColor(255,255,255,64 * alpha)

        love.graphics.draw(self.glow_img, self.center.x, self.center.y, 0, 1, 1, self.glow_img:getWidth()*0.5, self.glow_img:getHeight()*0.5)

        self.obstacles:draw()

        love.graphics.setColor(player.color[1], player.color[2], player.color[3], 255 * alpha)
        love.graphics.rectangle("fill", self.center.x - self.size.x * 0.5, self.center.y - self.size.y * 0.5, self.size.x, self.size.y)

        -- draw lifes
        local h = screenHeight
        for i = 0,self.lifes-1 do
            love.graphics.rectangle("fill", self.center.x - 5, h  - 60 + i * 15, 10, 10)
        end
    end


    player.keypressed = function(self, key)

        if key == self.keymap.left then
            self.dir = Vector(-1,0)
        end

        if key == self.keymap.right then
            self.dir = Vector(1,0)
        end

        if key == self.keymap.up then
            self.growth = -1
        end

        if key == self.keymap.down then
            self.growth = 1
        end
    end

    player.keyreleased = function(self, key)
        if key == self.keymap.left then
            self.dir.x = love.keyboard.isDown(self.keymap.right) and 1 or 0
        end
        if key == self.keymap.right then
            self.dir.x = love.keyboard.isDown(self.keymap.left) and -1 or 0
        end
        if key == self.keymap.up then
            self.growth = love.keyboard.isDown(self.keymap.down) and 1 or 0
        end
        if key == self.keymap.down then
            self.growth = love.keyboard.isDown(self.keymap.up) and -1 or 0
        end
    end

    player.joypressed = function(self, botan)
        if botan == 2 or botan == 4 then
            self.growth = 1
        elseif botan == 1 or botan == 3 then
            self.growth = -1
        end
    end

    player.joyreleased = function(self, botan)
        self.growth = 0
    end

    player.joyaxis = function(self, axis, value)
        if math.abs(value) < 0.5 then value = 0 end
        if axis == 1 then
            self.dir = Vector(value, 0)
        elseif axis == 2 then
            -- self.growth = value
        end
    end

    player.joyhat = function( self, hat, direction )
        if direction == "c" then
            self.dir = Vector(0,0)
            self.growth = 0
        end
        -- growth
        if direction == "u" or direction == "ru" or direction == "lu" then
            self.growth = -1
        end
        if direction == "d" or direction == "rd" or direction == "ld" then
            self.growth = 1
        end
        if direction == "l" or direction == "r" then
            self.growth = 0
        end

        -- move
        if direction == "r" or direction == "ru" or direction == "rd" then
            self.dir.x = 1
        end
        if direction == "l" or direction == "ld" or direction == "lu" then
            self.dir.x = -1
        end
        if direction == "u" or direction == "d" then
            self.dir.x = 0
        end
    end


    return player
end


function getSizeFromRatio(ratio)
    local w = 0.1 + 0.9 * ratio
    local h = 0.1 / w
    local base = 100
    return Vector(w * base,h * base)
end


