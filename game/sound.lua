--     Threaded: local multiplayer infinite runner game
--     Copyright (C) 2015 Christiaan Janssen

--     This program is free software: you can redistribute it and/or modify
--     it under the terms of the GNU General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.

--     This program is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU General Public License for more details.

--     You should have received a copy of the GNU General Public License
--     along with this program.  If not, see <http://www.gnu.org/licenses/>.

function initSound()
    Sound = {
        active = true,
    }

    love.audio.setVolume(1)

    if music and music:isPlaying() then music:stop() end
    if Sound.active then
        pcall(function() music = love.audio.newSource("snd/threaded_ost.ogg","stream") end)
        if music then
            music:setLooping(true)
            music:play()
        end
    end

    Sound.sfx = {
        crash = {},
        select = {},
        start = nil,
        win = nil,
        lose = nil,
    }

    for i=1,4 do
        pcall(function() Sound.sfx.crash[i] = love.audio.newSource("snd/crash_"..i..".ogg") end)
    end
    for i=1,4 do
        pcall(function() Sound.sfx.select[i] = love.audio.newSource("snd/select_"..i..".ogg") end)
    end
    pcall(function() Sound.sfx.start = love.audio.newSource("snd/start.ogg") end)
    pcall(function() Sound.sfx.win = love.audio.newSource("snd/winner.ogg") end)
    pcall(function() Sound.sfx.lose = love.audio.newSource("snd/game_over.ogg") end)
end

function playCrash(id)
    playSound(Sound.sfx.crash[id])
end

function playSelect(id)
    playSound(Sound.sfx.select[id])
end

function playSound(sound)
    if Sound.active and sound then
        sound:play()
    end
end

function toggleSound()
    love.audio.setVolume(1 - love.audio.getVolume())
end

function toggleMusic()
    if music and music:isPlaying() then
        music:pause()
    else
        music:play()
    end
end